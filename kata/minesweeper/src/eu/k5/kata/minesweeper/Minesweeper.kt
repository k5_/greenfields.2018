package eu.k5.kata.minesweeper

import java.util.*

fun main(args: Array<String>) {

}

class Minesweeper(
        val rows: Int,
        val columns: Int,
        private val cells: BitSet
) {


    fun mines(row: Int): String {
        val builder = StringBuilder()
        for (column in 0..columns) {

        }
        return builder.toString()
    }

    private fun isMine(row: Int, col: Int): Boolean {
        if (col !in 0 until columns) {
            return false
        }
        if (row !in 0 until rows) {
            return false
        }
        return cells.get(row * columns + col)
    }

    fun calculateHints(row: Int): String {
        val builder = StringBuilder(columns)
        for (column in 0 until columns) {
            if (isMine(row, column)) {
                builder.append("*")
            } else {
                builder.append(countMines(row, column))
            }
        }
        return builder.toString()
    }

    private fun countMines(row: Int, column: Int): Int {
        var count = 0
        if (isMine(row, column - 1)) {
            count++
        }
        if (isMine(row, column + 1)) {
            count++
        }
        if (isMine(row - 1, column)) {
            count++
        }
        if (isMine(row + 1, column)) {
            count++
        }
        if (isMine(row-1, column - 1)) {
            count++
        }
        if (isMine(row-1, column + 1)) {
            count++
        }
        if (isMine(row + 1, column-1)) {
            count++
        }
        if (isMine(row + 1, column+1)) {
            count++
        }

        return count
    }

    companion object {


        fun parse(lines: List<String>): Minesweeper {
            if (lines.isEmpty()) {
                throw IllegalArgumentException("Header missing")
            }
            val header = lines.get(0)
            val headerParts = header.split(" ")
            val rows = Integer.parseInt(headerParts[0])
            val columns = Integer.parseInt(headerParts[1])

            if (rows != lines.size - 1) {
                throw IllegalArgumentException("Wrong amount of lines. Rows = $rows, lines = " + lines.size)
            }
            val cells = BitSet()
            for (row in 0 until rows) {
                parse(row, columns, cells, lines.get(row + 1))
            }

            return Minesweeper(rows, columns, cells)
        }

        private fun parse(row: Int, columns: Int, cells: BitSet, line: String) {

            for (cell in line.toCharArray().withIndex()) {
                if ('*' == cell.value) {
                    cells[row * columns + cell.index] = true
                } else if ('.' == cell.value) {
                    cells[row * columns + cell.index] = false
                } else {
                    throw IllegalArgumentException("Not supported char in row $row")
                }
            }

        }
    }

}

class Board(
        val rows: Int,
        val columns: Int
) {

}