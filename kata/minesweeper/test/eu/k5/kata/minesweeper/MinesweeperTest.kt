package eu.k5.kata.minesweeper

import junit.framework.Assert.fail
import org.junit.Assert
import org.junit.Test

class MinesweeperTest {

    @Test
    fun noBoard() {
        try {
            parse()
            Assert.fail("empty Board should fail")
        } catch (exception: IllegalArgumentException) {

        }
    }

    @Test
    fun rowsInHeaderDoesNotMatchLineCount() {
        try {
            parse("2 1", "*")
            Assert.fail("rows(2) in header does not match line count(2 should be 3)")
        } catch (exception: IllegalArgumentException) {

        }
    }

    @Test
    fun singleMine_header() {
        val ms = parse("1 1", "*")
        Assert.assertEquals(1, ms.columns)
        Assert.assertEquals(1, ms.rows)
    }

    @Test
    fun singleCell_singleMine() {
        val ms = parse("1 1", "*")
        Assert.assertEquals("*", ms.calculateHints(0))
    }

    @Test
    fun singleCell_noMine() {
        val ms = parse("1 1", ".")
        Assert.assertEquals("0", ms.calculateHints(0))
    }

    @Test
    fun singleCell_wrongCharacter() {
        try {
            parse("1 1", "x")
            fail("'x' is not a valid character for minefield")
        } catch (exception: IllegalArgumentException) {

        }
    }

    @Test
    fun twoCells_singleMine() {
        val ms = parse("1 2", "*.")
        Assert.assertEquals("*1", ms.calculateHints(0))
    }


    @Test
    fun threeCells_noMineInTheMiddle() {
        val ms = parse("1 3", "*.*")
        Assert.assertEquals("*2*", ms.calculateHints(0))
    }


    @Test
    fun multipleColumns_singleMine() {
        val ms = parse("3 1", "*", ".", "*")
        Assert.assertEquals("*", ms.calculateHints(0))
        Assert.assertEquals("2", ms.calculateHints(1))
        Assert.assertEquals("*", ms.calculateHints(2))
    }

    @Test
    fun eightNeigbors() {
        val ms = parse("3 3", "***", "*.*", "***")
        Assert.assertEquals("***", ms.calculateHints(0))
        Assert.assertEquals("*8*", ms.calculateHints(1))
        Assert.assertEquals("***", ms.calculateHints(2))
    }


    fun parse(vararg lines: String): Minesweeper {
        return Minesweeper.Companion.parse(lines.asList())
    }

}