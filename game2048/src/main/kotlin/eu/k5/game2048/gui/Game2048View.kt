package eu.k5.game2048.gui

import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Label
import javafx.scene.input.KeyEvent
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox


private fun styleName(value: Int) = "label-$value"


class Game2048View(private val model: Game2048Model) {

    private val controller = Game2048Controller(model)

    fun asScene(): Scene {
        val scene = Scene(asNode(), 216.0, 263.0)
        scene.stylesheets.add("/eu/k5/game2048/gui/stylesheet.css")
        scene.onKeyPressed = EventHandler {
            controller.keyPress(it.code)
        }
        return scene
    }

    private fun createHeader(): HBox {
        val header: HBox = HBox()
        val scoreLabel = Label()
        scoreLabel.text = "Score"
        scoreLabel.styleClass.add("header")
        val scoreScore = Label()
        scoreScore.textProperty().bind(model.score.asString())
        scoreScore.styleClass.add("header")
        header.children.addAll(scoreLabel, scoreScore)
        return header
    }

    private fun asNode(): Parent {
        val pane = GridPane()
        model.fields.cellSet().forEach {
            val label = Label()
            val change = LabelChange(label)
            it.value!!.addListener(change)
            change.changed(it.value!!, 0, it.value!!.get())
            label.styleClass.add("piece-label")
            label.alignment = Pos.CENTER;

            pane.add(label, it.columnKey!!, it.rowKey!!)
        }
        pane.styleClass.add("grid")
        val vBox = VBox()
        vBox.children.addAll(createHeader(), pane)
        return vBox
    }

}


private class LabelChange(private val label: Label) : ChangeListener<Number> {
    override fun changed(observable: ObservableValue<out Number>, oldValue: Number?, newValue: Number?) {
        label.textProperty().set(newValue?.toString() ?: "")
        label.styleClass.remove(styleName(oldValue?.toInt() ?: 0))
        label.styleClass.add(styleName(newValue?.toInt() ?: 0))
    }
}