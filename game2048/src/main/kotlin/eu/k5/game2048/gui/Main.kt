package eu.k5.game2048.gui

import javafx.application.Application
import javafx.stage.Stage
import javafx.scene.Scene


fun main(args: Array<String>) {
    Application.launch(Game2048Application::class.java as Class<out Application>)
}

class Game2048Application : Application() {

    override fun start(primaryStage: Stage) {
        primaryStage.title = "2048"
        val model = Game2048Model()
        val view = Game2048View(model)


        primaryStage.scene = view.asScene()
        primaryStage.show()

        /**
        model.set(0, 0, 2)
        model.set(0, 1, 4)
        model.set(0, 2, 8)
        model.set(0, 3, 16)
        model.set(1, 0, 32)
        model.set(1, 1, 64)
        model.set(1, 2, 128)
        model.set(1, 3, 256)
        model.set(2, 0, 512)
        model.set(2, 1, 1024)
        model.set(2, 2, 2048)
        model.set(2, 3, 4096)
         **/

    }

}