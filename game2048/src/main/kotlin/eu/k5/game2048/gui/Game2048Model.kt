package eu.k5.game2048.gui

import com.google.common.collect.ArrayTable
import com.google.common.collect.Table
import eu.k5.game2048.Board
import eu.k5.game2048.GameBoard
import eu.k5.game2048.Move
import eu.k5.game2048.TableBoard
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleIntegerProperty
import java.util.*

class Game2048Model {

    val fields: Table<Int, Int, IntegerProperty> = ArrayTable.create(0..3, 0..3)

    var score = SimpleIntegerProperty()

    private var board: GameBoard = TableBoard()
    private val random = Random()

    init {
        for (row in 0..3) {
            for (col in 0..3) {
                fields.put(row, col, SimpleIntegerProperty())
            }
        }

        board.insertRandom(random)
        board.insertRandom(random)
        syncBoard()
    }

    fun move(move: Move) {
        if (board.move(move)) {
            board.insertRandom(random)
        }
        syncBoard()
    }

    private fun syncBoard() {
        fields.cellSet().forEach {
            it.value!!.set(board.get(it.rowKey!!, it.columnKey!!))
        }
        score.set(board.score())
    }

    fun set(row: Int, col: Int, value: Int) {
        board.set(row, col, value)
        syncBoard()
    }

}
