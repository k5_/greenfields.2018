package eu.k5.game2048.gui

import eu.k5.game2048.Move
import javafx.scene.input.KeyCode

class Game2048Controller(private val model: Game2048Model) {

    fun keyPress(code: KeyCode?) {
        when (code) {
            KeyCode.UP -> model.move(Move.UP)
            KeyCode.DOWN -> model.move(Move.DOWN)
            KeyCode.LEFT -> model.move(Move.LEFT)
            KeyCode.RIGHT -> model.move(Move.RIGHT)
        }
    }

}
