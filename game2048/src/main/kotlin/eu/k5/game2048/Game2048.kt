package eu.k5.game2048

import com.google.common.collect.ArrayTable
import com.google.common.collect.Table
import java.util.*


enum class Move(val horizontal: Boolean) {
    RIGHT(true), LEFT(true), UP(false), DOWN(false)
}

interface Board {

    fun set(row: Int, col: Int, value: Int): Board

    fun get(row: Int, col: Int): Int

    fun increaseScore(value: Int)

}

interface GameBoard : Board {

    fun score(): Int

    fun move(move: Move): Boolean

    fun insertRandom(random: Random)

}

private fun moveLeft(board: Board): Boolean {
    var changed = false
    for (row in 0..3) {
        var insertCol = 0
        var joined = false
        for (col in 0..3) {
            val value = board.get(row, col)
            if (value != 0) {
                if (!joined && insertCol > 0 && value == board.get(row, insertCol - 1)) {
                    board.set(row, insertCol - 1, value * 2)
                    board.increaseScore(value * 2)
                    joined = true
                    changed = true
                } else {
                    if (insertCol != col) {
                        changed = true
                    }
                    board.set(row, insertCol, value)
                    insertCol++
                    joined = false
                }
            }
        }
        for (col in insertCol..3) {
            board.set(row, col, 0)
        }
    }
    return changed
}

class TableBoard(private val table: Table<Int, Int, Int> = ArrayTable.create(0..3, 0..3)) : GameBoard {

    private var score: Int = 0

    override fun score(): Int {
        return score
    }

    override fun increaseScore(value: Int) {
        score += value
    }

    override fun insertRandom(random: Random) {
        val freeCells = freeCells()
        if (freeCells > 0) {
            val cell = random.nextInt(freeCells)
            val (row, col) = getFreeCell(cell)
            set(row, col, 2)
        }
    }

    private fun getFreeCell(number: Int): Pair<Int, Int> {
        var cellNumber = 0
        for (row in 0..3) {
            for (col in 0..3) {
                if (get(row, col) == 0) {
                    if (cellNumber == number) {
                        return Pair(row, col)
                    } else {
                        cellNumber++
                    }
                }
            }
        }
        throw  IllegalArgumentException("")
    }

    private fun freeCells(): Int {
        var freeCells = 0
        for (row in 0..3) {
            for (col in 0..3) {
                if (get(row, col) == 0) {
                    freeCells++
                }
            }
        }
        return freeCells
    }


    override fun move(move: Move): Boolean {
        val rotated =
                when (move) {
                    Move.LEFT -> Rotated(0, this)
                    Move.UP -> Rotated(90, this)
                    Move.RIGHT -> Rotated(180, this)
                    Move.DOWN -> Rotated(270, this)
                }

        return moveLeft(rotated)
    }


    override fun set(row: Int, col: Int, value: Int): Board {
        checkRange(row, col)
        table.put(row, col, value)
        return this
    }

    override fun get(row: Int, col: Int): Int {
        checkRange(row, col)
        return table.get(row, col) ?: 0
    }

    private fun checkRange(row: Int, col: Int) {
        if (row !in 0..3) {
            throw IllegalArgumentException("Row or Column not in range $row")
        }
        if (col !in 0..3) {
            throw IllegalArgumentException("Row or Column not in range: $col")
        }
    }

    override fun toString(): String {
        return table.toString()
    }

}

private class Rotated(private val degree: Int, private val board: Board) : Board {
    override fun increaseScore(value: Int) {
        board.increaseScore(value)
    }
    override fun get(row: Int, col: Int): Int {
        return board.get(applyRowRotation(row, col), applyColRotation(row, col))
    }

    override fun set(row: Int, col: Int, value: Int): Board {
        return board.set(applyRowRotation(row, col), applyColRotation(row, col), value)
    }

    private fun applyRowRotation(row: Int, col: Int): Int {
        return when (degree) {
            0 -> row
            90 -> col
            180 -> 3 - row
            270 -> 3 - col
            else -> throw IllegalArgumentException("Unsupported rotation")
        }
    }

    private fun applyColRotation(row: Int, col: Int): Int {
        return when (degree) {
            0 -> col
            90 -> row
            180 -> 3 - col
            270 -> 3 - row
            else -> throw IllegalArgumentException("Unsupported rotation")
        }
    }
}