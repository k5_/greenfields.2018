package eu.k5.game2048

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class Game2048Test {

    private var board: GameBoard = TableBoard()

    fun init() {

    }

    @Test
    fun moveRight_noJoin() {
        board.set(0, 0, 1)
        board.move(Move.RIGHT)
        assertEquals(1, board.get(0, 3))
        assertEquals(0, board.get(0, 2))
        assertEquals(0, board.get(0, 1))
        assertEquals(0, board.get(0, 0))
    }


    @Test
    fun moveRight_withJoin() {
        board.set(0, 0, 1).set(0, 1, 1)
        board.move(Move.RIGHT)
        assertEquals(2, board.get(0, 3))
        assertEquals(0, board.get(0, 2))
    }

    @Test
    fun moveRight_withDoubleJoin_doesNotDoubleJoin() {
        board.set(0, 0, 2).set(0, 1, 1).set(0, 2, 1)
        board.move(Move.RIGHT)
        assertEquals(2, board.get(0, 3))
        assertEquals(2, board.get(0, 2))
    }


    @Test
    fun moveLeft_noJoin() {
        board.set(0, 3, 1)
        board.move(Move.LEFT)
        assertEquals(1, board.get(0, 0))
    }


    @Test
    fun moveLeft_withJoin() {
        board.set(0, 3, 1).set(0, 2, 1)
        board.move(Move.LEFT)
        assertEquals(2, board.get(0, 0))
        assertEquals(0, board.get(0, 1))
    }

    @Test
    fun moveLeft_withDoubleJoin_doesNotDoubleJoin() {
        board.set(0, 3, 2).set(0, 2, 1).set(0, 1, 1)
        board.move(Move.LEFT)
        assertEquals(2, board.get(0, 0))
        assertEquals(2, board.get(0, 1))
    }


    @Test
    fun moveRight_noJoin_allRows() {
        board.set(0, 0, 1).set(1, 0, 2).set(2, 0, 3).set(3, 0, 4)
        board.move(Move.RIGHT)
        assertEquals(1, board.get(0, 3))
        assertEquals(2, board.get(1, 3))
        assertEquals(3, board.get(2, 3))
        assertEquals(4, board.get(3, 3))
    }

    @Test
    fun moveLeft_noJoin_allRows() {
        board.set(0, 3, 1).set(1, 3, 2).set(2, 3, 3).set(3, 3, 4)
        board.move(Move.LEFT)
        assertEquals(1, board.get(0, 0))
        assertEquals(2, board.get(1, 0))
        assertEquals(3, board.get(2, 0))
        assertEquals(4, board.get(3, 0))
    }

    @Test
    fun moveUp_noJoin_allCols() {
        board.set(3, 3, 1).set(3, 2, 2).set(3, 1, 3).set(3, 0, 4)
        board.move(Move.UP)
        print(board.toString())
        assertEquals(1, board.get(0, 3))
        assertEquals(2, board.get(0, 2))
        assertEquals(3, board.get(0, 1))
        assertEquals(4, board.get(0, 0))
    }

    @Test
    fun moveDown_noJoin_allCols() {
        board.set(0, 3, 1).set(0, 2, 2).set(0, 1, 3).set(0, 0, 4)
        board.move(Move.DOWN)
        print(board.toString())
        assertEquals(1, board.get(3, 3))
        assertEquals(2, board.get(3, 2))
        assertEquals(3, board.get(3, 1))
        assertEquals(4, board.get(3, 0))
    }
}