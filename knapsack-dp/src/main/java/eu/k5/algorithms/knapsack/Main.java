package eu.k5.algorithms.knapsack;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		solve();
	}

	private static List<Item> createItems() {
		List<Item> items = new ArrayList<>();
		items.add(new Item(10, 10));
		items.add(new Item(5, 5));
		items.add(new Item(4, 2));
		items.add(new Item(1, 1));
		return items;
	}

	public static void solve() {

		int maxWeight = 5;
		List<Item> items = createItems();
		/*
		 * Eingabe: U, B, w, v wie oben beschrieben R := [1�(n+1), 0�B]-Matrix,
		 * mit Eintr�gen 0 FOR i = n � 1
		 * 
		 * FOR j = 1 � B IF w(i) <= j R[i,j] := max( v(i) + R[i+1, j-w(i)],
		 * R[i+1,j] ) ELSE R[i,j] := R[i+1,j] Ausgabe: R[1,B]
		 */

		Keep keep = new Keep((maxWeight + 1), items.size());

		List<List<Integer>> all = new ArrayList<>();
		List<Integer> current = init(maxWeight);
		List<Integer> last = null;
		System.out.println("init");
		for (int item = items.size() - 1; item >= 0; item--) {

			last = current;
			all.add(current);
			current = init(maxWeight);

			for (int size = 1; size <= maxWeight; size++) {
				Item it = items.get(item);
				if (it.getWeight() <= size) {

					int newValue = last.get(size - it.getWeight()) + it.getValue();
					if (newValue > last.get(size)) {

						keep.set(item, size, true);
						current.set(size, newValue);

					} else {

						keep.set(item, size, false);

						current.set(size, last.get(size));
					}
				} else {

					keep.set(item, size, false);

					current.set(size, last.get(size));
				}
			}
		}
		Set<Integer> usedItems = new HashSet<>();
		int w = maxWeight;
		for (int item = 0 ; item < items.size(); item++) {
			if (keep.get(item, w)) {
				usedItems.add(item);
				w = w - items.get(item).getWeight();
			}
		}

		System.out.println(usedItems);
		System.out.println(all);
	}

	static class Keep {
		private final int width;
		private final BitSet bitSet;

		public Keep(int width, int height) {
			this.width = width;
			bitSet = new BitSet(width * height);
		}

		public boolean get(int x, int y) {
			return bitSet.get(x * width + y);
		}

		public void set(int x, int y, boolean value) {
			bitSet.set(x * width + y, value);
		}
	}

	private static List<Integer> init(int values) {
		List<Integer> list = new ArrayList<>();
		for (int index = 0; index <= values; index++) {
			list.add(0);
		}
		return list;
	}
}
