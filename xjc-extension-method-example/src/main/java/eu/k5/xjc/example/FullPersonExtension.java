package eu.k5.xjc.example;

import eu.k5.tr.model.FullPerson;
import eu.k5.xjc.extender.Extension;

public class FullPersonExtension {

	
	@Extension
	public static void extensionMethod(FullPerson person) {
		System.out.println(person.getPost());
	}
}
