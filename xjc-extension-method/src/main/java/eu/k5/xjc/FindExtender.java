package eu.k5.xjc;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

public class FindExtender {

	public static void main(String[] args) {

		System.out.println("start");
		CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
		combinedTypeSolver.add(new ReflectionTypeSolver());

		// Configure JavaParser to use type resolution
		JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedTypeSolver);
		StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);

		// Parse some code
		CompilationUnit cu = StaticJavaParser.parse("class X { static @abc.Extension int x(int x) { return 1 + 1.0 - 5; } }");

		cu.findAll(MethodDeclaration.class).forEach(method -> {
			for (Modifier modifier : method.getModifiers()) {
				System.out.println("modifier " + modifier.toString());
			}
			for (AnnotationExpr ann : method.getAnnotations()) {
				System.out.println("annotation " + ann.getName());

			}
			for (Parameter paramter : method.getParameters()) {
				System.out.println("paramter " + paramter.getNameAsString());
			}
			System.out.println(method.getType());

		});

	}
	
	
	
	
	public static void parse() {
		CompilationUnit cu = StaticJavaParser.parse("class X { static @abc.Extension int x(int x) { return 1 + 1.0 - 5; } }");

	}
}
