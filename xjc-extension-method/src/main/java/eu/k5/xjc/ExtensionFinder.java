package eu.k5.xjc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.ImportDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.type.ClassOrInterfaceType;
import com.github.javaparser.ast.type.Type;
import com.github.javaparser.metamodel.ClassOrInterfaceTypeMetaModel;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;

import eu.k5.xjc.extender.Extension;

public class ExtensionFinder {

	private final static ExtensionFinder INSTANCE = new ExtensionFinder("");

	private String directory;

	private List<Path> parse;

	private Map<String, List<MethodDeclaration>> findMethods;

	public static ExtensionFinder getInstance() {
		return INSTANCE;
	}

	public ExtensionFinder(String directory) {
		this.directory = directory;

		try {
			parse = parse();

			findMethods = findMethods(parse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Map<String, List<MethodDeclaration>> findMethods(List<Path> paths) {

		CombinedTypeSolver combinedTypeSolver = new CombinedTypeSolver();
		combinedTypeSolver.add(new ReflectionTypeSolver());

		// Configure JavaParser to use type resolution
		JavaSymbolSolver symbolSolver = new JavaSymbolSolver(combinedTypeSolver);
		StaticJavaParser.getConfiguration().setSymbolResolver(symbolSolver);

		Map<String, List<MethodDeclaration>> methods = new HashMap<>();
		for (Path path : paths) {
			CompilationUnit cu;
			try {
				cu = StaticJavaParser.parse(path);
				for (ImportDeclaration imp : cu.getImports()) {
					System.out.println(imp.getName());
				}
				cu.findAll(MethodDeclaration.class).forEach(m -> extractExtenstionMethod(m, methods));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return methods;
	}

	private static void extractExtenstionMethod(MethodDeclaration method,
			Map<String, List<MethodDeclaration>> methods) {
		if (!isExtensionMethod(method)) {
			return;
		}
		if (method.getParameters().isEmpty()) {
			System.out.println("no p");
		} else {

			Parameter parameter = method.getParameter(0);
			Type type = parameter.getType();
			if (type instanceof ClassOrInterfaceType) {
				ClassOrInterfaceType classOrInterfaceType = (ClassOrInterfaceType) type;

				ClassOrInterfaceTypeMetaModel metaModel = classOrInterfaceType.getMetaModel();

				classOrInterfaceType.

						String qualifiedName = metaModel.getQualifiedClassName();

				methods.computeIfAbsent(qualifiedName, (__) -> new ArrayList<>()).add(method);

			} else {
				System.out.println("unsupported first parameter");
			}

		}

	}

	private static boolean isExtensionMethod(MethodDeclaration method) {

		Optional<AnnotationExpr> annotation = method.getAnnotationByClass(Extension.class);
		return annotation.isPresent();
	}

	public Map<String, List<MethodDeclaration>> getMethods() {
		return findMethods;
	}

	public static List<Path> parse() throws IOException {

		FileFinder finder = new FileFinder();

		Files.walkFileTree(Paths.get("src/main/java"), finder);

		return finder.getJavaFiles();
	}

	public static class FileFinder extends SimpleFileVisitor<Path> {

		private List<Path> javaFiles = new ArrayList<>();

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			if (file.getFileName().toString().endsWith(".java")) {
				javaFiles.add(file);
			}
			return FileVisitResult.CONTINUE;
		}

		public List<Path> getJavaFiles() {
			return javaFiles;
		}
	}
}
