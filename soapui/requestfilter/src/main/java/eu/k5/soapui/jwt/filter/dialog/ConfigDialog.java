package eu.k5.soapui.jwt.filter.dialog;

import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.SwingUtilities;

public class ConfigDialog {
	public static void main(String[] args) {
		Path path = Paths.get("target").resolve("test.xml");

		final ConfigDialogModel model = new ConfigDialogModel(path);

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				model.display();
			}
		});
	}

}
