package eu.k5.soapui.jwt.filter;

import com.eviware.soapui.impl.support.definition.InterfaceDefinitionPart;
import com.eviware.soapui.impl.wsdl.WsdlInterface;
import com.eviware.soapui.impl.wsdl.WsdlRequest;
import com.eviware.soapui.plugins.ActionConfiguration;
import com.eviware.soapui.plugins.ToolbarPosition;
import com.eviware.soapui.support.action.support.AbstractSoapUIAction;

@ActionConfiguration(actionGroup = "WsdlRequestActions", toolbarPosition = ToolbarPosition.NONE, toolbarIcon = "/favicon.png", description = "COnfigSays Hello")
public class TolerantRepair extends AbstractSoapUIAction<WsdlRequest> {

	public TolerantRepair() {
		super("Repair", "Configures JWT");
	}

	@Override
	public void perform(WsdlRequest arg0, Object arg1) {
		WsdlInterface interface1 = arg0.getOperation().getInterface();
		try {
			for (InterfaceDefinitionPart part : interface1.getDefinitionContext().getDefinitionParts()) {
				System.out.println(part.getType() + " " + part.getUrl() + " " + part.getContent());

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("repair");
	}

}
