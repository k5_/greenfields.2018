package eu.k5.soapui.jwt.filter.provider;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class JwtClient {

	private final String endpoint;
	private final String requestBody;
	private final HttpClient httpClient;

	private JwtClient(HttpClient httpClient, Builder builder) {
		this.httpClient = httpClient;
		this.endpoint = builder.endpoint;
		this.requestBody = builder.requestBody;
	}

	public String invoke() {

		HttpPost post = new HttpPost();
		//post.setHeader(name, value);

		try {
			HttpResponse response = httpClient.execute(post);

			return "";
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		private String endpoint;
		private String requestBody;

		private byte[] keystore;
		private String keystorePassword;

		public Builder withEndpoint(String endpoint) {
			this.endpoint = endpoint;
			return this;
		}

		public Builder withRequestBody(String requestBody) {
			this.requestBody = requestBody;
			return this;
		}

		public Builder withKeystore(byte[] keystore) {
			this.keystore = keystore;
			return this;
		}

		public Builder withKeystorePassword(String keystorePassword) {
			this.keystorePassword = keystorePassword;
			return this;
		}

		public JwtClient build() {
			HttpClient httpClient;
			if (keystore == null) {
				httpClient = createHttpClient();
			} else {
				httpClient = createCertHttpClient();
			}

			return new JwtClient(httpClient, this);
		}

		private HttpClient createCertHttpClient() {

			return new DefaultHttpClient();
		}

		private void getKeystore() {
			try {
				// TrustManagerFactory tmf =
				// TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				KeyStore ks = KeyStore.getInstance("JKS");
				ks.load(new ByteArrayInputStream(keystore), keystorePassword.toCharArray());
	
			} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// tmf.init(ks);
		}

		private HttpClient createHttpClient() {
			return new DefaultHttpClient();
		}

	}
}
