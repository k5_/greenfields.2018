package eu.k5.soapui.jwt.filter.provider;

import org.junit.Assert;
import org.junit.Test;

import eu.k5.soapui.jwt.filter.provider.TokenProvider.Token;

public class TokenExtractorTest {

	@Test
	public void test() {
		TokenExtractor extractor = new TokenExtractor(".token[0]", ".expires_in[0]");

		Token extract = extractor.extract("{ \"token\":\"abc\", \"expires_in\":400}");
		Assert.assertEquals(400, extract.getExpires());
	}
}
